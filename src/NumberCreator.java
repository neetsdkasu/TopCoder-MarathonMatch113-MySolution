import java.io.*;
import java.math.*;
import java.util.*;

public class NumberCreator {
    public static void main(String[] args) throws Exception {
        Main.main(args);
    }

    LimitTimer timer;
    BigInteger num0, num1, target;

    public String[] findSolution(int Num0, int Num1, String T) {
        timer = new LimitTimer(8500L);

        System.err.println("num0: " + Num0);
        System.err.println("num1: " + Num1);
        System.err.println("T.length: " + T.length());

        num0 = BigInteger.valueOf((long)Num0);
        num1 = BigInteger.valueOf((long)Num1);
        target = new BigInteger(T);

        String[] ret = solveSqrt();
        String sel = "sqrt";
        System.err.println("sqrt_end: " + timer.progress() + " ms");

        IntList[] list70 = gen70(Num0, Num1);

        String[] tmp = testPoors(list70);
        System.err.println("poor_end: " + timer.progress() + " ms");
        if (ret == null || (tmp != null && tmp.length < ret.length)) {
            ret = tmp;
            sel = "poor";
        }

        tmp = fakeBruteForce(6);
        System.err.println("fake_end: " + timer.progress() + " ms");
        if (ret == null || (tmp != null && tmp.length < ret.length)) {
            ret = tmp;
            sel = "fake";
        }

        System.err.println("select: " + sel);

        return ret;
    }

    String[] solveSqrt() {
        ArrayList<int[]> relations = new ArrayList<>();
        ArrayList<BigInteger> nums = new ArrayList<>();
        IntList list = new IntList(2000);
        HashMap<BigInteger, Integer> ind = new HashMap<>();
        TreeSet<BigInteger> set = new TreeSet<>();

        ind.put(num0, nums.size());
        nums.add(num0);
        relations.add(new int[2]);

        ind.put(num1, nums.size());
        nums.add(num1);
        relations.add(new int[2]);

        ind.put(target, nums.size());
        nums.add(target);
        relations.add(new int[2]);
        set.add(target);

        while (!set.isEmpty()) {
            BigInteger e = set.pollLast();
            // System.err.println(" pollLast: " + e);
            int e_id = ind.get(e);
            list.add(e_id);
            if (BigInteger.ONE.equals(e)) {
                break;
            }
            BigInteger[] rd = sqrt(e);
            if (rd == null) {
                // System.err.println("what? " + e);
                continue;
            }
            BigInteger rd0 = rd[0].signum() < 0 ? rd[0].abs() : rd[0];
            if (!ind.containsKey(rd0)) {
                ind.put(rd0, nums.size());
                nums.add(rd0);
                relations.add(new int[2]);
                set.add(rd0);
            }
            if (!ind.containsKey(rd[1])) {
                ind.put(rd[1], nums.size());
                nums.add(rd[1]);
                relations.add(new int[2]);
                set.add(rd[1]);
            }
            BigInteger sq = rd[1].pow(2);
            if (!ind.containsKey(sq)) {
                ind.put(sq, nums.size());
                nums.add(sq);
                relations.add(new int[2]);
                set.add(sq);
            }
            int rd0_id = ind.get(rd0);
            int rd1_id = ind.get(rd[1]);
            int[] rel = relations.get(e_id);
            rel[0] = rd[0].signum() < 0 ? (rd0_id^(-1)) : rd0_id;
            rel[1] = rd1_id;
        }

        // Display expressions
        // for (int i = 0; i < list.size(); i++) {
            // int e_id = list.get(i);
            // if (BigInteger.ONE.equals(nums.get(e_id))) {
                // System.err.println("[" + i + "] " + nums.get(e_id));
                // System.err.println("  " + num0 + " / " + num0);
                // break;
            // }
            // int[] rel = relations.get(e_id);
            // int rd0_id = rel[0] < 0 ? (rel[0]^(-1)) : rel[0];
            // BigInteger e0 = nums.get(rd0_id);
            // BigInteger e1 = nums.get(rel[1]);
            // if (e0.signum() == 0) {
                // System.err.println("[" + i + "] " + nums.get(e_id));
                // System.err.println("  " + e1 + " ^2");
                // continue;
            // }
            // BigInteger sq = e1.pow(2);
            // String op = rel[0] < 0 ? " + " : " - ";
            // System.err.println("[" + i + "] " + nums.get(e_id));
            // System.err.println("  " + sq + op + e0);
        // }


        int[] new_ids = new int[nums.size()];
        int[] counts = new int[nums.size()];
        IntList[] graph = new IntList[nums.size()];
        int[][] exprs = new int[nums.size()][];

        new_ids[ind.get(num0)] = 0;
        new_ids[ind.get(num1)] = 1;
        for (int i = 0; i < graph.length; i++) {
            graph[i] = new IntList(nums.size());
        }

        for (int i = 0; i < list.size(); i++) {
            int e_id = list.get(i);
            int[] rel = relations.get(e_id);
            exprs[e_id] = rel;
            if (BigInteger.ONE.equals(nums.get(e_id))) {
                counts[e_id]++;
                graph[ind.get(num0)].add(e_id);
                continue;
            }
            int diff_id = rel[0] < 0 ? (rel[0]^(-1)) : rel[0];
            int sqrt_id = rel[1];
            BigInteger diff_num = nums.get(diff_id);
            BigInteger sqrt_num = nums.get(sqrt_id);
            if (diff_num.signum() == 0) {
                counts[e_id]++;
                graph[sqrt_id].add(e_id);
            } else {
                BigInteger square = sqrt_num.pow(2);
                int square_id = ind.get(square);
                counts[e_id] += 2;
                graph[diff_id].add(e_id);
                graph[square_id].add(e_id);
            }
        }

        ArrayList<String> ret = new ArrayList<>();
        IntList
            que1 = new IntList(nums.size()),
            que2 = new IntList(nums.size()),
            queSwap = null;
        for (int i = 0; i < counts.length; i++) {
            if (counts[i] == 0) {
                que1.add(i);
            }
        }
        int id_count = 2;
        // System.err.println(">> id[0]: " + num0);
        // System.err.println(">> id[1]: " + num1);
        while (que1.size() > 0) {
            que2.clear();
            for (int i = 0; i < que1.size(); i++) {
                int e_id = que1.get(i);
                BigInteger e_num = nums.get(e_id);
                if (e_num != num0 && e_num != num1 && e_num.signum() != 0) {
                    // System.err.println("new id[" + id_count + "]: " + e_num);
                    new_ids[e_id] = id_count;
                    id_count++;
                    int[] rel = exprs[e_id];
                    if (BigInteger.ONE.equals(e_num)) {
                        ret.add("0 / 0");
                        // System.err.println(">> [" + new_ids[e_id] + "] = [0] / [0]");
                        // System.err.println(">> 1 = " + num0 + " / " + num0);
                    } else if (rel != null) {
                        int diff_id = rel[0] < 0 ? (rel[0]^(-1)) : rel[0];
                        int sqrt_id = rel[1];
                        BigInteger diff_num = nums.get(diff_id);
                        BigInteger sqrt_num = nums.get(sqrt_id);
                        if (diff_num.signum() == 0) {
                            int sqrt_new_id = new_ids[sqrt_id];
                            ret.add(String.format("%d * %d", sqrt_new_id, sqrt_new_id));
                            // System.err.println(">> [" + new_ids[e_id] + "] = [" + sqrt_new_id + "] * [" + sqrt_new_id + "]");
                            // System.err.println(">> " + e_num + " = " + sqrt_num + " * " + sqrt_num);
                        } else {
                            BigInteger square_num = sqrt_num.pow(2);
                            int square_id = ind.get(square_num);
                            int square_new_id = new_ids[square_id];
                            int diff_new_id = new_ids[diff_id];
                            String op = rel[0] < 0 ? " + " : " - ";
                            ret.add(String.format("%d" + op + "%d", square_new_id, diff_new_id));
                            // System.err.println(">> [" + new_ids[e_id] + "] = [" + square_new_id + "]" + op + "[" + diff_new_id + "]");
                            // System.err.println(">> " + e_num + " = " + square_num + op + diff_num);
                        }
                    }
                }
                IntList conn = graph[e_id];
                for (int jj = 0; jj < conn.size(); jj++) {
                    int c_id = conn.get(jj);
                    counts[c_id]--;
                    if (counts[c_id] == 0) {
                        que2.add(c_id);
                    }
                }
            }
            queSwap = que1; que1 = que2; que2 = queSwap;
        }

        // simulate(ret.toArray(new String[0]));

        return ret.toArray(new String[0]);
    }

    ArrayList<BigInteger> simulate(String[] ret) {
        ArrayList<BigInteger> list = new ArrayList<>(ret.length + 2);
        list.add(num0);
        list.add(num1);
        for (String r : ret) {
            System.err.println(r);
            String[] ss = r.split(" ");
            int a = Integer.parseInt(ss[0]);
            int b = Integer.parseInt(ss[2]);
            switch (ss[1].charAt(0)) {
            case '+': list.add(list.get(a).add(list.get(b))); break;
            case '-': list.add(list.get(a).subtract(list.get(b))); break;
            case '*': list.add(list.get(a).multiply(list.get(b))); break;
            case '/': list.add(list.get(a).divide(list.get(b))); break;
            }
        }
        return list;
    }

    void testSqrts() {
        BigInteger[] bis = sqrt(target);
        if (bis != null) {
            BigInteger[] bis2 = sqrt(bis);
            if (bis2 != null) {
                System.err.println("bis[0].len: " + bis[0].abs().bitLength());
                System.err.println("bis[1].len: " + bis[1].bitLength());
                for (int i = 0; i < bis2.length; i++) {
                    System.err.println("bis2[" + i + "].len: " + bis2[i].abs().bitLength());
                }
                BigInteger[] bis3 = sqrt(bis[0].abs());
                BigInteger[] bis4 = sqrt(bis[1].abs());
                if (bis3 != null) {
                    System.err.println("bis3[0].len: " + bis3[0].abs().bitLength());
                    System.err.println("bis3[1].len: " + bis3[1].bitLength());
                }
                if (bis4 != null) {
                    System.err.println("bis4[0].len: " + bis4[0].abs().bitLength());
                    System.err.println("bis4[1].len: " + bis4[1].bitLength());
                }
                if (target.bitLength() < 64) {
                    System.err.println("target: " + target);
                    System.err.println("bis[0]: " + bis[0]);
                    System.err.println("bis[1]: " + bis[1]);
                    System.err.println("bis[1]^2: " + bis[1].pow(2));
                    for (int i = 0; i < bis2.length; i++) {
                        System.err.println("bis2[" + i + "]: " + bis2[i]);
                    }
                    System.err.println("bis2[" + (bis2.length - 1) + "]^2: " + bis2[bis2.length - 1].pow(2));
                    if (bis3 != null) {
                        System.err.println("bis3[0]: " + bis3[0]);
                        System.err.println("bis3[1]: " + bis3[1]);
                        System.err.println("bis3[1]^2: " + bis3[1].pow(2));
                    }
                    if (bis4 != null) {
                        System.err.println("bis4[0]: " + bis4[0]);
                        System.err.println("bis4[1]: " + bis4[1]);
                        System.err.println("bis4[1]^2: " + bis4[1].pow(2));
                    }
                }
            }
        }
    }

    BigInteger[] sqrt(BigInteger[] values) {
        int hb = 0;
        for (int i = 0; i < values.length; i++) {
            hb = Math.max(hb, values[i].bitLength());
        }
        int mb = (hb + 1) / 2;
        BigInteger cur = BigInteger.ZERO;
        BigInteger min_diff = null, best = null;
        ArrayList<BigInteger>
            best_diffs = new ArrayList<>(values.length),
            diffs = new ArrayList<>(values.length),
            for_swap = null;
        for (int b = mb; b >= 0; b--) {
            BigInteger tmp = cur.setBit(b);
            BigInteger sq = tmp.pow(2);
            BigInteger diff = null;
            diffs.clear();
            int sig = 0;
            for (int i = 0; i < values.length; i++) {
                BigInteger tmp_diff = sq.subtract(values[i]);
                sig += sq.signum();
                diffs.add(tmp_diff);
                if (diff == null || tmp_diff.abs().compareTo(diff.abs()) > 0) {
                    diff = tmp_diff;
                }
            }
            if (min_diff == null || diff.abs().compareTo(min_diff.abs()) < 0) {
                best = tmp;
                min_diff = diff;
                for_swap = diffs; diffs = best_diffs; best_diffs = for_swap;
            }
            if (sig <= 0) {
                cur = tmp;
            }
        }
        if (best == null) {
            return null;
        } else {
            best_diffs.add(best);
            return best_diffs.toArray(new BigInteger[0]);
        }
    }

    BigInteger[] sqrt(BigInteger value) {
        int hb = value.bitLength();
        int mb = (hb + 1) / 2;
        BigInteger cur = BigInteger.ZERO;
        BigInteger min_diff = null, best = null;
        for (int b = mb; b >= 0; b--) {
            BigInteger tmp = cur.setBit(b);
            BigInteger sq = tmp.pow(2);
            BigInteger diff = sq.subtract(value);
            if (min_diff == null || diff.abs().compareTo(min_diff.abs()) <= 0) {
                best = tmp;
                min_diff = diff;
            }
            if (diff.signum() == 0) {
                break;
            } else if (diff.signum() < 0) {
                cur = tmp;
            }
        }
        if (best == null) {
            return null;
        } else {
            return new BigInteger[]{min_diff, best};
        }
    }

    void testSqrt(BigInteger value) {
        BigInteger[] ret = sqrt(value);
        System.err.println("valueBits: " + value.bitLength());
        if (ret == null) {
            System.err.println("no such a square root fo value");
            return;
        }
        BigInteger best = ret[1], min_diff = ret[0];
        System.err.println("sqrtBits: " + best.bitLength());
        System.err.println("diffBits: " + min_diff.bitLength());
        if (value.bitLength() < 64) {
            System.err.println("value: " + value);
            System.err.println("sqrt: " + best);
            System.err.println("sqrt^2: " + best.pow(2));
            System.err.println("diff: " + min_diff);
        }
    }

    String[] testPoors(IntList[] list70) {
        LimitTimer timer = this.timer.makeIntervalLimitTimer(7000);
        int best_i = 0, best_len = 300, best_retlen = 300;
        ArrayList<BigInteger> dummyBi = new ArrayList<>(100);
        ArrayList<String> dummySt = new ArrayList<>(100);
        BigInteger[] pool = new BigInteger[71];
        int len_max = 0;
        for (int i = 1; i < pool.length; i++) {
            pool[i] = BigInteger.valueOf((long)i);
            len_max = Math.max(len_max, list70[i].size());
        }
        for (int i = 0; i < len_max; i++) {
            dummySt.add("");
        }
        for (int i = 20; i < 50; i++) {
            if (timer.isOver()) { break; }
            IntList ls = list70[i];
            dummyBi.clear();
            int pos = 0;
            for (int j = 2; j < ls.size(); j++) {
                int v = ls.get(j);
                if (v < pool.length) {
                    dummyBi.add(pool[v]);
                } else {
                    dummyBi.add(BigInteger.valueOf((long)v));
                }
                if (v == i) { pos = j; }
            }
            String[] ret = poorSolve(pos, pool[i], null, 300, dummyBi, dummySt.subList(0, dummyBi.size()), best_len);
            if (ret != null && ret.length < best_len) {
                best_i = i;
                best_len = ret.length;
            }
        }
        for (int i = 50; i <= 70; i++) {
            if (timer.isOver()) { break; }
            IntList ls = list70[i];
            dummyBi.clear();
            int pos = 0;
            for (int j = 2; j < ls.size(); j++) {
                int v = ls.get(j);
                if (v < pool.length) {
                    dummyBi.add(pool[v]);
                } else {
                    dummyBi.add(BigInteger.valueOf((long)v));
                }
                if (v == i) { pos = j; }
            }
            String[] ret = poorSolve(pos, pool[i], null, 300, dummyBi, dummySt.subList(0, dummyBi.size()), best_len);
            if (ret != null && ret.length < best_len) {
                best_i = i;
                best_len = ret.length;
            }
        }
        for (int i = 2; i < 20; i++) {
            if (timer.isOver()) { break; }
            IntList ls = list70[i];
            dummyBi.clear();
            int pos = 0;
            for (int j = 2; j < ls.size(); j++) {
                int v = ls.get(j);
                if (v < pool.length) {
                    dummyBi.add(pool[v]);
                } else {
                    dummyBi.add(BigInteger.valueOf((long)v));
                }
                if (v == i) { pos = j; }
            }
            String[] ret = poorSolve(pos, pool[i], null, 300, dummyBi, dummySt.subList(0, dummyBi.size()), best_len);
            if (ret != null && ret.length < best_len) {
                best_i = i;
                best_len = ret.length;
            }
        }
        // System.err.println("best_i: " + best_i);
        // System.err.println("best_len: " + best_len);
        if (best_i == 0) { return null; }
        IntList best = list70[best_i];
        ArrayList<BigInteger> addNums = new ArrayList<>();
        ArrayList<String> addRet = new ArrayList<>();
        BigInteger se = BigInteger.valueOf((long)best_i);
        int pos = 0;
        for (int i = 0; i < best.size(); i++) {
            int e = best.get(i);
            if (e == best_i) { pos = i; }
            if (i < 2) { continue; }
            addNums.add(BigInteger.valueOf((long)e));
        }
    loop:
        for (int i = best.size() - 1; i > 1; i--) {
            int e = best.get(i);
            for (int j = 0; j < i; j++) {
                int n = best.get(j);
                for (int k = j; k < i; k++) {
                    int m = best.get(k);
                    if (n + m == e) {
                        addRet.add(String.format("%d + %d", j, k));
                        continue loop;
                    }
                    if (n - m == e) {
                        addRet.add(String.format("%d - %d", j, k));
                        continue loop;
                    }
                    if (m - n == e) {
                        addRet.add(String.format("%d - %d", k, j));
                        continue loop;
                    }
                    if (n * m == e) {
                        addRet.add(String.format("%d * %d", j, k));
                        continue loop;
                    }
                    if (n / m == e) {
                        addRet.add(String.format("%d / %d", j, k));
                        continue loop;
                    }
                    if (m / n == e) {
                        addRet.add(String.format("%d / %d", k, j));
                        continue loop;
                    }
                }
            }
        }
        Collections.reverse(addRet);
        return poorSolve(pos, se, null, 300, addNums, addRet, 300);
    }

    IntList[] gen70(int n0, int n1) {
        int count = 0;
        IntList[] table = new IntList[Math.max(n0, n1) * 3];
        table[n0] = new IntList(2);
        table[n0].add(n0);
        table[n0].add(n1);
        table[n1] = table[n0];
        if (n0 <= 70) { count++; }
        if (n1 <= 70) { count++; }
        IntList tmp = new IntList(table.length), tmp2 = new IntList(table.length), tmp3;
        tmp.add(n0);
        tmp.add(n1);
        while (count < 70 && tmp.size() > 0) {
            tmp2.clear();
            for (int i = 0; i < tmp.size(); i++) {
                int n = tmp.get(i);
                IntList ist = table[n];
                for (int j = 0; j < ist.size(); j++) {
                    int m = ist.get(j);
                    int x = n + m;
                    if (0 < x && x < table.length && table[x] == null) {
                        table[x] = ist.getCopyAndAdd(x);
                        tmp2.add(x);
                        if (x <= 70) { count++; }
                    }
                    x = Math.abs(n - m);
                    if (0 < x && x < table.length && table[x] == null) {
                        table[x] = ist.getCopyAndAdd(x);
                        tmp2.add(x);
                        if (x <= 70) { count++; }
                    }
                    x = n * m;
                    if (0 < x && x < table.length && table[x] == null) {
                        table[x] = ist.getCopyAndAdd(x);
                        tmp2.add(x);
                        if (x <= 70) { count++; }
                    }
                    x = n / m;
                    if (0 < x && x < table.length && table[x] == null) {
                        table[x] = ist.getCopyAndAdd(x);
                        tmp2.add(x);
                        if (x <= 70) { count++; }
                    }
                    x = m / n;
                    if (0 < x && x < table.length && table[x] == null) {
                        table[x] = ist.getCopyAndAdd(x);
                        tmp2.add(x);
                        if (x <= 70) { count++; }
                    }
                }
            }
            tmp3 = tmp; tmp = tmp2; tmp2 = tmp3;
        }
        return table;
    }

    String[] poors() {

        LimitTimer timer = this.timer.makeIntervalLimitTimer(6000);

        String[] ret = poorSolve(0, num0, null, 2000);
        int min = ret != null ? ret.length : 2000;
        if (timer.isOver()) { /* System.err.println("over"); */ return ret; }

        String[] tmp = poorSolve(1, num1, null, 2000);
        if (ret == null || (tmp != null && tmp.length < ret.length)) {
            ret = tmp;
            min = ret.length;
        }
        if (timer.isOver()) { /* System.err.println("over"); */ return ret; }

        tmp = poorSolve(2, num0.add(num1), "0 + 1", min);
        if (ret == null || (tmp != null && tmp.length < ret.length)) {
            ret = tmp;
            min = ret.length;
        }
        if (timer.isOver()) { /* System.err.println("over"); */ return ret; }

        tmp = poorSolve(2, num0.multiply(num1), "0 * 1", min);
        if (ret == null || (tmp != null && tmp.length < ret.length)) {
            ret = tmp;
            min = ret.length;
        }
        if (timer.isOver()) { /* System.err.println("over"); */ return ret; }

        tmp = poorSolve(2, num0.subtract(num1), "0 - 1", min);
        if (ret == null || (tmp != null && tmp.length < ret.length)) {
            ret = tmp;
            min = ret.length;
        }
        if (timer.isOver()) { /* System.err.println("over"); */ return ret; }

        tmp = poorSolve(2, num1.subtract(num0), "1 - 0", min);
        if (ret == null || (tmp != null && tmp.length < ret.length)) {
            ret = tmp;
            min = ret.length;
        }
        if (timer.isOver()) { /* System.err.println("over"); */ return ret; }

        tmp = poorSolve(2, num0.divide(num1), "0 / 1", min);
        if (ret == null || (tmp != null && tmp.length < ret.length)) {
            ret = tmp;
            min = ret.length;
        }
        if (timer.isOver()) { /* System.err.println("over"); */ return ret; }

        tmp = poorSolve(2, num1.divide(num0), "1 / 0", min);
        if (ret == null || (tmp != null && tmp.length < ret.length)) {
            ret = tmp;
            min = ret.length;
        }

        return ret;
    }

    String[] poorSolve(int spos, BigInteger se, String ses, int min) {
        return poorSolve(spos, se, ses, min, null, null, min);
    }

    String[] poorSolve(int spos, BigInteger se, String ses, int min, int best) {
        return poorSolve(spos, se, ses, min, null, null, best);
    }

    String[] poorSolve(int spos, BigInteger se, String ses, int min, List<BigInteger> addNums, List<String> addRet, int best) {

        // System.err.println("gen: from " + spos + ", " + se + ", " + String.valueOf(ses));

        if (se.signum() <= 0 || BigInteger.ONE.equals(se)) {
            return null;
        }

        ArrayList<String> ret = new ArrayList<>();
        ArrayList<BigInteger> nums = new ArrayList<>();

        nums.add(num0);
        nums.add(num1);

        if (ses != null) {
            nums.add(se);
            ret.add(ses);
        }
        if (addNums != null) {
            nums.addAll(addNums);
            ret.addAll(addRet);
        }

        int pfirst = nums.size() - 1;

        // if (!se.equals(nums.get(spos))) { throw new RuntimeException("HA?"); }
        int tpos = spos;
        while (target.compareTo(nums.get(tpos)) > 0) {
            nums.add(nums.get(tpos).multiply(nums.get(spos)));
            ret.add(String.format("%d * %d", tpos, spos));
            if (nums.size() > min) { return null; }
            tpos = nums.size() - 1;
        }

        if (target.equals(nums.get(nums.size() - 1))) {
            // System.out.println("foundit!! len " + ret.size());
            return ret.toArray(new String[0]);
        }

        int plast = nums.size() - 1;

        nums.add(BigInteger.ONE);
        ret.add("1 / 1");
        int one = nums.size() - 1;
        int ise = se.intValue(); // System.err.println("ies: " + ise);
        for (int i = 2; i < ise; i++) {
            int j = nums.size() - 1;
            nums.add(nums.get(j).add(nums.get(one)));
            ret.add(String.format("%d + %d", j, one));
            if (nums.size() > min) { return null; }
        }

        int ilast = nums.size() - 1;

        boolean[] used = new boolean[nums.size()];
        BigInteger r = target;
        List<BigInteger> view = nums.subList(one, ilast + 1);
        for (int ix = plast; ix >= pfirst; ix--) {
            int ii = ix > pfirst ? ix : spos;
            BigInteger d = r.divide(nums.get(ii));
            if (d.signum() == 0) {
                continue;
            }
            used[ii] = true;
            int j = Collections.binarySearch(view, d);
            if (j >= 0) {
                j += one;
                nums.add(nums.get(j).multiply(nums.get(ii)));
                ret.add(String.format("%d * %d", j, ii));
                r = r.remainder(nums.get(ii));
                view = nums.subList(one, ilast + 1);
                used[j] = true;
            } else {
                // System.err.println("WHY!? " + d + ", " + (ix - pfirst));
            }
            if (nums.size() > min) { return null; }
        }


        int pos = nums.size() - 2;
        while (pos >= 0 && target.compareTo(nums.get(nums.size() - 1)) > 0) {
            int i = nums.size() - 1;
            if (pos <= ilast) {
                if (!r.equals(nums.get(pos))) {
                    pos--;
                    continue;
                }
            }
            nums.add(nums.get(i).add(nums.get(pos)));
            ret.add(String.format("%d + %d", i, pos));
            pos--;
            if (nums.size() > min) { return null; }
        }

        if (!target.equals(nums.get(nums.size() - 1))) {
            // System.err.println("why failed!?");
            return null;
        }

        if (nums.size() < 300) {
            used[0] = true;
            used[1] = true;
            used = Arrays.copyOf(used, nums.size());
            used[used.length - 1] = true;
            boolean ok = true;
            IntList ixx = new IntList(used.length * 3);
            int count = 2;
            for (int i = nums.size() - 1; i > 1; i--) {
                if (!used[i]) { continue; }
                count++;
                if (count >= best) {
                    ok = false;
                    break;
                }
                BigInteger t = nums.get(i);
                int bt = t.bitLength();
                int best_j = 0, best_k = 0, best_u = 0, best_p = nums.size(), op = 0;
                for (int j = i - 1; j >= 0; j--) {
                    BigInteger e0 = nums.get(j);
                    int b0 = e0.bitLength();
                    for (int k = j; k >= 0; k--) {
                        BigInteger e1 = nums.get(k);
                        int b1 = e1.bitLength();
                        int d = Math.max(b0, b1) - bt;
                        if (Math.abs(d) < 2 && t.equals(e0.add(e1))) {
                            int u = (used[j] ? 1 : 0) + (used[k] ? 1 : 0), p = Math.max(j, k);
                            if (u > best_u || (u == best_u && p < best_p)) {
                                best_j = j; best_k = k; best_u = u; best_p = p; op = 0;
                            }
                        }
                        if (d >= 0) {
                            BigInteger ee = e0.subtract(e1);
                            if (t.equals(ee.abs())) {
                                int u = (used[j] ? 1 : 0) + (used[k] ? 1 : 0), p = Math.max(j, k);
                                if (u > best_u || (u == best_u && p < best_p)) {
                                    best_j = j; best_k = k; best_u = u; best_p = p; op = ee.signum();
                                }
                            }
                        }
                        d = (b0 + b1) - bt;
                        if (Math.abs(d) < 2 && t.equals(e0.multiply(e1))) {
                            int u = (used[j] ? 1 : 0) + (used[k] ? 1 : 0), p = Math.max(j, k);
                            if (u > best_u || (u == best_u && p < best_p)) {
                                best_j = j; best_k = k; best_u = u; best_p = p; op = 2;
                            }
                        }
                        d = (b0 - b1) - bt;
                        if (Math.abs(d) < 2 && t.equals(e0.divide(e1))) {
                            int u = (used[j] ? 1 : 0) + (used[k] ? 1 : 0), p = Math.max(j, k);
                            if (u > best_u || (u == best_u && p < best_p)) {
                                best_j = j; best_k = k; best_u = u; best_p = p; op = 3;
                            }
                        }
                        d = (b1 - b0) - bt;
                        if (e0 != e1 && Math.abs(d) < 2 && t.equals(e1.divide(e0))) {
                            int u = (used[j] ? 1 : 0) + (used[k] ? 1 : 0), p = Math.max(j, k);
                            if (u > best_u || (u == best_u && p < best_p)) {
                                best_j = j; best_k = k; best_u = u; best_p = p; op = 4;
                            }
                        }
                    }
                }
                if (best_p < nums.size()) {
                    used[best_j] = true;
                    used[best_k] = true;
                    ixx.add(best_j);
                    ixx.add(best_k);
                    ixx.add(op);
                } else {
                    // System.err.println("failed " + i + " / " + nums.size());
                    // if (i < 10) {
                        // for (int j = 0; j <= i; j++) {
                            // System.err.print(" " + nums.get(j));
                        // }
                        // System.err.println();
                    // } else {
                        // System.err.println("num0: " + num0);
                        // System.err.println("num1: " + num1);
                        // System.err.println("numi: " + t);
                    // }
                    ok = false;
                    break;
                }
            }
            if (ok) {
                int count2 = 0;
                int[] ind = new int[used.length];
                for (int i = 0; i < used.length; i++) {
                    if (used[i]) {
                        ind[i] = count2;
                        count2++;
                    }
                }
                ret.clear();
                for (int i = 0; i < ixx.size(); i += 3) {
                    int j = ind[ixx.get(i)];
                    int k = ind[ixx.get(i + 1)];
                    int op = ixx.get(i + 2);
                    if (op < 0) {
                        ret.add(String.format("%d - %d", k, j));
                    } else {
                        switch (op) {
                        case 0: ret.add(String.format("%d + %d", j, k)); break;
                        case 1: ret.add(String.format("%d - %d", j, k)); break;
                        case 2: ret.add(String.format("%d * %d", j, k)); break;
                        case 3: ret.add(String.format("%d / %d", j, k)); break;
                        case 4: ret.add(String.format("%d / %d", k, j)); break;
                        }
                    }
                }
                Collections.reverse(ret);
                // System.err.println("orig: " + nums.size() + ", use: " + count2 + ", " + (count == count2));
            }
        }

        // System.err.println("foundit!, len " + ret.size());

        return ret.toArray(new String[0]);
    }

    ArrayList<BigInteger> putToMapForBF(ArrayList<BigInteger> ix, HashMap<BigInteger, ArrayList<BigInteger>> map, ArrayList<BigInteger> list, BigInteger e) {
        if (e.signum() > 0 && !map.containsKey(e)) {
            ArrayList<BigInteger> list2 = new ArrayList<>(list.size() + 1);
            list2.addAll(list);
            list2.add(e);
            map.put(e, list2);
            ix.add(e);
            if (target.equals(e)) {
                return list2;
            }
        }
        return null;
    }

    String[] makeRetForBF(ArrayList<BigInteger> list) {
        ArrayList<String> ret = new ArrayList<>(list.size() - 2);
    loop:
        for (int i = 2; i < list.size(); i++) {
            BigInteger e = list.get(i);
            for (int j = 0; j < i; j++) {
                BigInteger e0 = list.get(j);
                for (int k = j; k < i; k++) {
                    BigInteger e1 = list.get(k);
                    if (e.equals(e0.add(e1))) {
                        ret.add(String.format("%d + %d", j, k));
                        continue loop;
                    }
                    if (e.equals(e0.subtract(e1))) {
                        ret.add(String.format("%d - %d", j, k));
                        continue loop;
                    }
                    if (e.equals(e1.subtract(e0))) {
                        ret.add(String.format("%d - %d", k, j));
                        continue loop;
                    }
                    if (e.equals(e0.multiply(e1))) {
                        ret.add(String.format("%d * %d", j, k));
                        continue loop;
                    }
                    if (e.equals(e0.divide(e1))) {
                        ret.add(String.format("%d / %d", j, k));
                        continue loop;
                    }
                    if (e.equals(e1.divide(e0))) {
                        ret.add(String.format("%d / %d", k, j));
                        continue loop;
                    }
                }
            }
        }
        return ret.toArray(new String[0]);
    }

    String[] fakeBruteForce(int maxDepth) {
        HashMap<BigInteger, ArrayList<BigInteger>> map = new HashMap<>();
        {
            ArrayList<BigInteger> list = new ArrayList<>(2);
            list.add(num0);
            list.add(num1);
            map.put(num0, list);
            map.put(num1, list);
        }
        ArrayList<BigInteger>
            ix1 = new ArrayList<>(300000),
            ix2 = new ArrayList<>(300000),
            ix3 = null;
        ix1.add(num0);
        ix1.add(num1);
        LimitTimer timer = this.timer.makeIntervalLimitTimer(8000);
        for (int depth = 0; depth < maxDepth; depth++) {
            ix2.clear();
            for (int k = 0; k < ix1.size(); k++) {
                if (timer.isOver()) { return null; }
                BigInteger key = ix1.get(k);
                ArrayList<BigInteger> list = map.get(key), ret;
                for (int i = 0; i < list.size(); i++) {
                    BigInteger e0 = list.get(i);
                    for (int j = i; j < list.size(); j++) {
                        BigInteger e1 = list.get(j);
                        ret = putToMapForBF(ix2, map, list, e0.add(e1));
                        if (ret != null) { return makeRetForBF(ret); }
                        ret = putToMapForBF(ix2, map, list, e0.subtract(e1).abs());
                        if (ret != null) { return makeRetForBF(ret); }
                        ret = putToMapForBF(ix2, map, list, e0.multiply(e1));
                        if (ret != null) { return makeRetForBF(ret); }
                        ret = putToMapForBF(ix2, map, list, e0.divide(e1));
                        if (ret != null) { return makeRetForBF(ret); }
                        if (e0 != e1) {
                            ret = putToMapForBF(ix2, map, list, e1.divide(e0));
                            if (ret != null) { return makeRetForBF(ret); }
                        }
                    }
                }
            }
            ix3 = ix1; ix1 = ix2; ix2 = ix3;
            // System.err.println("[" + (depth + 1) + "]map.size: " + map.size());
        }
        return null;
    }

}

class IntList {
    int[] buf = null;
    int index = 0;
    private IntList() {}
    IntList(int cap) {
        buf = new int[cap];
    }
    int get(int i) {
        return buf[i];
    }
    int size() {
        return index;
    }
    void add(int v) {
        buf[index] = v;
        index++;
    }
    void set(int i, int v) {
        buf[i] = v;
    }
    void clear() {
        index = 0;
    }
    IntList getCopyAndAdd(int v) {
        IntList ret = new IntList();
        ret.buf = Arrays.copyOf(buf, Math.max(buf.length, index + 1));
        ret.index = index + 1;
        ret.buf[index] = v;
        return ret;
    }
}

class LimitTimer {
    long startTime;
    long limitTime;
    LimitTimer(long limit) {
        startTime = System.currentTimeMillis();
        limitTime = startTime + limit;
    }
    long progress() {
        return System.currentTimeMillis() - startTime;
    }
    boolean isOver() {
        return System.currentTimeMillis() > limitTime;
    }
    LimitTimer makeIntervalLimitTimer(long limit) {
        return new IntervalLimitTimer(limit, limitTime);
    }
    static class IntervalLimitTimer extends LimitTimer {
        long interval = 0, time0;
        private IntervalLimitTimer(long limit, long totalLimit) {
            super(limit);
            limitTime = Math.min(limitTime, totalLimit);
            time0 = startTime;
        }
        @Override
        boolean isOver() {
            long time1 = System.currentTimeMillis();
            interval = Math.max(interval, time1 - time0);
            time0 = time1;
            return time1 + interval > limitTime;
        }
    }
}


class Main {

    static int callFindSolution(BufferedReader in, NumberCreator numberCreator) throws Exception {

        int Num0 = Integer.parseInt(in.readLine());
        int Num1 = Integer.parseInt(in.readLine());
        String T = in.readLine();

        long time0 = System.currentTimeMillis();
        String[] _result = numberCreator.findSolution(Num0, Num1, T);
        long time1 = System.currentTimeMillis();
        System.err.println("time: " + (time1 - time0) + " ms");

        System.out.println(_result.length);
        for (String _it : _result) {
            System.out.println(_it);
        }
        System.out.flush();

        return 0;
    }

    public static void main(String[] args) throws Exception {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        NumberCreator numberCreator = new NumberCreator();

        callFindSolution(in, numberCreator);

    }
}
