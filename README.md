TopCoder Marathon Match 113 My Solution
=======================================


Problem Statement   
https://www.topcoder.com/challenges/30110632   


Standings  
https://www.topcoder.com/challenges/30110632?tab=submissions   


Review Page (New platform)  
https://submission-review.topcoder.com/challenges/30110632  


Review Page (Old platform)  
https://software.topcoder.com/review/actions/ViewProjectDetails?pid=30110632  


Forum  
http://forums.topcoder.com/?module=Category&categoryID=86487  


